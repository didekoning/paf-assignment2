grammar RichRail;

options {
  language = Java;
}

@members {
public CommandHandler handler;
}

@rulecatch {
}

command 	    : newcommand | addcommand | getcommand | delcommand | remcommand;

newcommand 	    : newtraincommand | newwagoncommand;
newtraincommand : 'new' 'train' ID {handler.newTrainCommand($ID.text);};
newwagoncommand	: 'new' 'wagon' ID ('numseats' numseats=NUMBER)? {handler.newWagonCommand($ID.text, $numseats.text);};
addcommand 	    : 'add' add=ID 'to' to=ID {handler.addCommand($add.text, $to.text);};
getcommand 	    : 'getnumseats' type ID {handler.getCommand($type.text, $ID.text);};
delcommand 	    : 'delete' type ID {handler.delCommand($type.text, $ID.text);};
remcommand	    : 'remove' remove=ID 'from' from=ID {handler.remCommand($remove.text, $from.text);};

type		    : ('train') | ('wagon');

ID		        : ('a'..'z')('a'..'z'|'0'..'9')*;
NUMBER		    : ('0'..'9')+;
WS              : ( '\t' | ' ' | '\r' | '\n'| '\u000C')+ -> skip;