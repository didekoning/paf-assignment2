package domain;

public class Wagon {
    private String wagonID;
    private int capacity;
    private WagonType type;

    public Wagon(String wagonID, int capacity, WagonType type){
        this.wagonID = wagonID;
        this.capacity = capacity;
        this.type = type;
    }

    public String getWagonID() {
        return wagonID;
    }

    public WagonType getType() {
        return type;
    }

    public int getCapacity() {
        return capacity;
    }

    public String printCapacity() {
        return String.format("number of %s in %s: %s", type.getMeasure(), wagonID, capacity);
    }

    public String getMeasure() {
        return type.getMeasure();
    }

    public String toString(){
        return wagonID;
    }
}
