package domain;

import java.util.LinkedList;
import java.util.List;

public class Train {
    private String trainID;
    private List<Wagon> wagons = new LinkedList<>();

    public Train(String trainID){
        this.trainID = trainID;
    }

    public String getTrainID() {
        return trainID;
    }

    public List<Wagon> getWagons() {
        return wagons;
    }

    public void addWagon(Wagon wagon){
        wagons.add(wagon);
    }

    public void removeWagon(Wagon wagon){
        wagons.remove(wagon);
    }

    public Wagon searchWagon(String wagonID){
        for (Wagon wagon : wagons) {
            if (wagon.getWagonID().equals(wagonID)) return wagon;
        }

        return null;
    }

    public String printCapacity() {
        int totalCapacity = 0;
        for (Wagon w : wagons) totalCapacity += w.getCapacity();
        return String.format("number of seats in train %s: %s", trainID, totalCapacity);
    }

    @Override
    public String toString() {
        return this.trainID;
    }
}
