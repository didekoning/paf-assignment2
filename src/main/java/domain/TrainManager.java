package domain;

import gui.GuiController;
import logging.Context;
import logging.logMessage;
import persistence.TrainService;

import java.util.List;

public class TrainManager {
    private final TrainService SERVICE = TrainService.getTrainService();
    private GuiController guiController;
    Context context = new Context(new logMessage());

    public TrainManager(GuiController guiController) {
        this.guiController = guiController;
    }

    public void newTrain(String trainID) {
        Train tr = new Train(trainID);

        if (SERVICE.getTrainByID(trainID) != null) {
            sendLog(String.format("train %s already exists", trainID));
            return;
        }

        SERVICE.saveTrain(tr);
        sendLog(String.format("train %s created", trainID));
    }

    public void newWagon(String wagonID, String numseats, WagonType type) {
        if (numseats == null) numseats = "20";
        Wagon wg = new Wagon(wagonID, Integer.parseInt(numseats), type);

        if (SERVICE.getWagonByID(wagonID) != null) {
            sendLog(String.format("wagon %s already exists", wagonID));
            return;
        }

        SERVICE.saveWagon(wg);
        sendLog(String.format("wagon %s created", wagonID));
    }

    public void addWagonToTrain(String trainID, String wagonID) {
        Train tr = SERVICE.getTrainByID(trainID);
        if (tr == null) {
            sendLog(String.format("train %s does not exists", trainID));
            return;
        }

        Wagon wg = SERVICE.getWagonByID(wagonID);
        if (wg == null) {
            sendLog(String.format("wagon %s does not exists", wagonID));
            return;
        }

        tr.addWagon(wg);
        SERVICE.saveTrain(tr);
        sendLog(String.format("wagon %s added to train %s", wagonID, trainID));
    }

    public void removeWagonFromTrain(String trainID, String wagonID) {
        Train tr = SERVICE.getTrainByID(trainID);
        if (tr == null) {
            sendLog(String.format("train %s does not exists", trainID));
            return;
        }

        Wagon wg = tr.searchWagon(wagonID);
        if (wg == null) {
            sendLog(String.format("wagon %s does not exists", wagonID));
            return;
        }

        tr.removeWagon(wg);
        SERVICE.saveTrain(tr);
        sendLog(String.format("wagon %s removed from %s", wagonID, trainID));
    }

    public void getCapacity(String type, String id) {
        if (type.equals("train")) {
            Train tr = SERVICE.getTrainByID(id);
            if (tr == null) {
                sendLog(String.format("train %s does not exists", id));
                return;
            }

            sendLog(tr.printCapacity());
        } else {
            Wagon wg = SERVICE.getWagonByID(id);
            if (wg == null) {
                sendLog(String.format("wagon %s does not exists", id));
                return;
            }

            sendLog(wg.printCapacity());
        }
    }

    public void delete(String type, String id) {
        if (type.equals("train")) {
            Train tr = SERVICE.getTrainByID(id);
            if (tr == null) {
                sendLog(String.format("train %s does not exists", id));
                return;
            }

            SERVICE.deleteTrain(tr);
            sendLog(String.format("train %s deleted", id));
        } else {
            for (Train train : SERVICE.getAllTrains()) {
                if (train.searchWagon(id) != null) {
                    sendLog("wagon is assigned to one ore more trains");
                    return;
                }
            }

            Wagon wg = SERVICE.getWagonByID(id);
            if (wg == null) {
                sendLog(String.format("wagon %s does not exists", id));
                return;
            }

            SERVICE.deleteWagon(wg);
            sendLog(String.format("wagon %s deleted", id));
        }
    }

    private void sendLog(String message){
        guiController.update(message);
        context.executeStrategy(message);
    }

    public List<Train> getTrains() {
        return SERVICE.getAllTrains();
    }

    public Train getTrainByID(String trainID) {
        return SERVICE.getTrainByID(trainID);
    }

    public List<Wagon> getWagons() {
        return SERVICE.getAllWagons();
    }

    public Wagon getWagonByID(String wagonID) {
        return SERVICE.getWagonByID(wagonID);
    }
}
