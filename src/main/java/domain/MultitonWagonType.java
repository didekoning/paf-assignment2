package domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultitonWagonType {
    private static Map<String, WagonType> types = new HashMap<>();

    static {
        types.put("passenger", new Passenger());
    }

    public static WagonType getType(String key) {
        return types.get(key);
    }

    public static List<WagonType> getAllTypes() {
        return new ArrayList<>(types.values());
    }
}
