package domain;

public class Passenger implements WagonType {
    private String name = "passenger";
    private String measure = "seats";

    @Override
    public String getMeasure() {
        return measure;
    }

    public String toString() {
         return name;
    }
}
