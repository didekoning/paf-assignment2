package gui;

import domain.Train;
import domain.Wagon;
import domain.WagonType;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class RichRail implements GuiModel {
    private GuiController guiController;

    private JTabbedPane tabbedPane1;
    private JPanel richrail;
    private JTextPane commandOutput;
    private JTextPane trainOverview;
    private JTextField commandInput;
    private JButton execCommand;
    private JComboBox trainList;
    private JTextField selectedTextField;
    private JTextField trainName;
    private JButton makeNewTrainButton;
    private JButton selectTrainButton;
    private JButton deleteTrainButton;
    private JButton addWagonButton;
    private JButton deleteWagonButton;
    private JComboBox wagonTypeList;
    private JTextField wagonNaamTextField;
    private JComboBox trainWagonsList;
    private JPanel trainPanel;
    private JLabel errorLabel;
    private JTextField seatsTextField;
    private JLabel trainLabel;
    private Train selectedTrain;

    public RichRail() {
        guiController = new GuiController(this);

        buildGui();
    }

    public void initListeners(){
        execCommand.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addLineCommandOutput(commandInput.getText());
                guiController.runCommand(commandInput.getText());
            }
        });

        selectTrainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        deleteTrainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedTextField.setText("selected");
                selectedTrain = null;
                guiController.deleteTrain((Train)trainList.getSelectedItem());
            }
        });

        selectTrainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(trainList.getSelectedItem() == null){
                    errorLabel.setText("Geen trein geselecteerd!");
                }else{
                    selectTrain((Train)trainList.getSelectedItem());
                    drawTrain();
                }

            }
        });

        makeNewTrainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!trainName.getText().isEmpty()){
                    guiController.newTrain(trainName.getText());
                }else{
                    errorLabel.setText("Treinnaam is leeg!");
                }
            }
        });

        addWagonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selectedTrain.getTrainID().isEmpty()){
                    errorLabel.setText("Geen trein geselecteerd!");
                }else{
                    guiController.addWagonToTrain(selectedTrain, wagonNaamTextField.getText(), seatsTextField.getText(), (WagonType)wagonTypeList.getSelectedItem());
                }

            }
        });

        deleteWagonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(trainWagonsList.getSelectedItem() != null){
                    guiController.removeWagonFromTrain(selectedTrain, (Wagon)trainWagonsList.getSelectedItem());
                }else {
                    errorLabel.setText("Geen wagon geselecteerd!");
                }
            }
        });
    }

    public void addLineCommandOutput(String message) {
        commandOutput.setText(String.format("%s%s\n", commandOutput.getText(), message));
    }

    @Override
    public void update() {
        initGui();
    }

    private void buildGui(){
        JFrame frame = new JFrame("RichRail");
        frame.setContentPane(richrail);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        frame.setVisible(true);

        initListeners();
        initGui();
    }

    private void initGui(){
        errorLabel.setText("");
        loadTrains();
        loadWagons();
        drawTrain();
        loadWagonTypes();
        trainDetail();
    }
    private void selectTrain(Train train){
        selectedTextField.setText(train.getTrainID());
        this.selectedTrain = train;
        loadWagons();
        drawTrain();
    }

    private void loadTrains(){
        trainList.removeAllItems();
        List<Train> trains = guiController.getTrains();
        for(Train t : trains){
            trainList.addItem(t);
        }
    }

    private void loadWagons(){
        trainWagonsList.removeAllItems();
        if(selectedTrain != null){
            List<Wagon> wagons = selectedTrain.getWagons();
            for(Wagon w : wagons){
                trainWagonsList.addItem(w);
            }
        }
    }

    private void loadWagonTypes() {
        wagonTypeList.removeAllItems();
        for (WagonType wt : guiController.getWagonTypes()) {
            wagonTypeList.addItem(wt);
        }
    }

    private void drawTrain() {
        trainPanel.revalidate();
        trainPanel.repaint();
        trainPanel.removeAll();

        if(selectedTrain != null) {
            BufferedImage trainImg = null;
            BufferedImage wagonImg = null;
            try {
                trainImg = ImageIO.read(new File(System.getProperty("user.dir") + "\\resources\\train.png"));
                wagonImg = ImageIO.read(new File(System.getProperty("user.dir") + "\\resources\\wagon.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            trainPanel.setLayout(new GridBagLayout());

            JLabel trainLabel = new JLabel();
            trainLabel.setMaximumSize(new Dimension(127, 102));
            trainLabel.setIcon(new javax.swing.ImageIcon(trainImg));
            trainPanel.add(trainLabel);

            List<Wagon> wagons = selectedTrain.getWagons();
            if(wagons.size() > 0){
                for (Wagon w : wagons) {
                    JLabel wagonLabel = new JLabel();
                    wagonLabel.setMaximumSize(new Dimension(127, 102));
                    wagonLabel.setIcon(new javax.swing.ImageIcon(wagonImg));
                    trainPanel.add(wagonLabel);
                }
            }

        }
    }

    private void trainDetail() {
        trainOverview.revalidate();
        trainOverview.repaint();
        trainOverview.removeAll();

        trainOverview.setText("WAGONS:\n");
        for (Wagon w : guiController.getWagons()) {
            trainOverview.setText(String.format("%s(%s:%s) ", trainOverview.getText(), w.getWagonID(), w.getCapacity()));
        }

        trainOverview.setText(trainOverview.getText() + "\nTRAINS:\n");
        for (Train t : guiController.getTrains()) {
            trainOverview.setText(String.format("%s(%s)", trainOverview.getText(), t.getTrainID()));

            for (Wagon w : t.getWagons()) {
                trainOverview.setText(String.format("%s-(%s)", trainOverview.getText(), w.getWagonID()));
            }

            trainOverview.setText(trainOverview.getText() + "\n");
        }
    }
}
