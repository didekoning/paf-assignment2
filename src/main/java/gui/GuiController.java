package gui;

import cli.Cli;
import domain.*;

import java.util.List;

public class GuiController {
    private GuiModel gui;
    private TrainManager trainManager;
    private Cli cli = new Cli();

    public void update(String message) {
        gui.addLineCommandOutput(message);
        gui.update();
    }

    public GuiController(GuiModel model){
        trainManager = new TrainManager(this);
        gui = model;
    }

    public void runCommand(String command) {
        cli.exec(trainManager ,command);
    }

    public List<Train> getTrains() {
        return trainManager.getTrains();
    }

    public List<Wagon> getWagons() {
        return trainManager.getWagons();
    }

    public void removeWagonFromTrain(Train train, Wagon wagon){
        trainManager.removeWagonFromTrain(train.getTrainID(), wagon.getWagonID());
    }

    public void newTrain(String trainID){
        trainManager.newTrain(trainID);
    }

    public void addWagonToTrain(Train train, String wagonID, String numseats, WagonType type){
        if (trainManager.getWagonByID(wagonID) == null) {
            trainManager.newWagon(wagonID, numseats, type);
        }

        trainManager.addWagonToTrain(train.getTrainID(), wagonID);
    }

    public void deleteTrain(Train train){
        trainManager.delete("train", train.getTrainID());
    }

    public List<WagonType> getWagonTypes() {
        return MultitonWagonType.getAllTypes();
    }
}
