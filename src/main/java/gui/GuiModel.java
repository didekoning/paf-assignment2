package gui;

public interface GuiModel {
    void update();
    void addLineCommandOutput(String message);
}
