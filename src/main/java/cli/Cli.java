package cli;

import domain.MultitonWagonType;
import domain.TrainManager;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class Cli implements cli.CommandHandler {
    private TrainManager handler;

    public void exec(TrainManager handler, String command) {
        this.handler = handler;

        // Split string into tokens
        CharStream input = new ANTLRInputStream(command);
        CommonTokenStream tokens = new CommonTokenStream(new RichRailLexer(input));

        // Parser generates abstract syntax tree
        RichRailParser parser = new RichRailParser(tokens);

        parser.handler = this;

        try {
            parser.command();
        } catch (Exception e) {
            System.out.println("Invalid command");
        }
    }

    @Override
    public void newTrainCommand(String id) {
        handler.newTrain(id);
    }

    @Override
    public void newWagonCommand(String id, String numseats) {
        handler.newWagon(id, numseats, MultitonWagonType.getType("passenger"));
    }

    @Override
    public void addCommand(String add, String to) {
        handler.addWagonToTrain(to, add);
    }

    @Override
    public void getCommand(String type, String id) {
        handler.getCapacity(type, id);
    }

    @Override
    public void delCommand(String type, String id) {
        handler.delete(type, id);
    }

    @Override
    public void remCommand(String remove, String from) {
        handler.removeWagonFromTrain(from, remove);
    }
}
