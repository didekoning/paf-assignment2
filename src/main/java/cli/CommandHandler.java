package cli;

public interface CommandHandler {
    void newTrainCommand(String id);

    void newWagonCommand(String id, String numseats);

    void addCommand(String add, String to);

    void getCommand(String type, String id);

    void delCommand(String type, String id);

    void remCommand(String remove, String from);
}