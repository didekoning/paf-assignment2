package persistence;

import domain.Wagon;

import java.util.List;

public interface WagonDAO {
    void save(Wagon wagon);
    void delete(String wagonID);
    Wagon findByKey(String wagonID);
    List<Wagon> findAll();
}
