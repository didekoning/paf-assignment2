package persistence;

import domain.Train;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrainMockImpl implements TrainDAO {
    private final Map<String, Train> trains = new HashMap<>();

    @Override
    public void save(Train train) {
        trains.put(train.getTrainID(), train);
    }

    @Override
    public void delete(String trainID) {
        trains.remove(trainID);
    }

    @Override
    public Train findByKey(String trainID) {
        return trains.get(trainID);
    }

    @Override
    public List<Train> findAll() {
        return new ArrayList<>(trains.values());
    }
}
