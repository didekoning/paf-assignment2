package persistence;

import domain.Train;
import domain.Wagon;

import java.util.List;

public class TrainService {
    private static final TrainService TRAIN_SERVICE = new TrainService();

    private final TrainDAO TRAIN_DAO = new TrainMockImpl();
    private final WagonDAO WAGON_DAO  = new WagonMockImpl();

    public static TrainService getTrainService() {
        return TRAIN_SERVICE;
    }

    public Train getTrainByID(String trainID) {
        return TRAIN_DAO.findByKey(trainID);
    }

    public List<Train> getAllTrains() {
        return TRAIN_DAO.findAll();
    }

    public void saveTrain(Train train) {
        TRAIN_DAO.save(train);
    }

    public void deleteTrain(Train train) {
        TRAIN_DAO.delete(train.getTrainID());
    }

    public Wagon getWagonByID(String wagonID) {
        return WAGON_DAO.findByKey(wagonID);
    }

    public List<Wagon> getAllWagons() {
        return WAGON_DAO.findAll();
    }

    public void saveWagon(Wagon wagon) {
        WAGON_DAO.save(wagon);
    }

    public void deleteWagon(Wagon wagon) {
        WAGON_DAO.delete(wagon.getWagonID());
    }
}
