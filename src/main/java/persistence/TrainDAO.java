package persistence;

import domain.Train;

import java.util.List;

public interface TrainDAO {
    void save(Train train);
    void delete(String trainID);
    Train findByKey(String trainID);
    List<Train> findAll();
}
