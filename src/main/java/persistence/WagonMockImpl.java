package persistence;

import domain.Wagon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WagonMockImpl implements WagonDAO {
    private final Map<String, Wagon> wagons = new HashMap<>();

    @Override
    public void save(Wagon wagon) {
        wagons.put(wagon.getWagonID(), wagon);
    }

    @Override
    public void delete(String wagonID) {
        wagons.remove(wagonID);
    }

    @Override
    public Wagon findByKey(String wagonID) {
        return wagons.get(wagonID);
    }

    @Override
    public List<Wagon> findAll() {
        return new ArrayList<>(wagons.values());
    }
}
