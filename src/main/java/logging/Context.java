package logging;

/**
 * Created by Dion on 12/29/2016.
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    public void executeStrategy(String message){
        strategy.logThis(message);
    }
}
