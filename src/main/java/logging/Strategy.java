package logging;

/**
 * Created by Dion on 12/29/2016.
 */
public interface Strategy {
    public void logThis(String message);
}
